import { NextApiRequest, NextApiResponse } from "next";
import { dbConnect } from "../../../server/dbconnect";
import { Dog } from "../../../server/entities/Dog";

export default async function handler(
    req:NextApiRequest, 
    res:NextApiResponse<Dog[]>) {
    const connection = await dbConnect();
    
    res.json(await connection.getRepository<Dog>('Dog').find());
}