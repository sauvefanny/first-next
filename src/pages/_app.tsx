import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css'
import type { AppProps } from 'next/app'
import axios from 'axios'
import { Navigation } from '../components/Navigation';

axios.defaults.baseURL = 'http://localhost:3000'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Navigation></Navigation>
      <Component {...pageProps} />
    </>
  );
}
export default MyApp
