import { Container, Navbar, Nav } from "react-bootstrap";
import Link from 'next/link';


export function Navigation() {

    return (
        <Navbar bg="light" expand="lg">
            <Container>

                <Link href="/" passHref>
                    <Navbar.Brand>First Next</Navbar.Brand>
                </Link>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Link href="/" passHref>
                            <Nav.Link>
                                Home
                            </Nav.Link>
                        </Link>
                        <Link href="/dog" passHref>
                            <Nav.Link>
                                Dogs
                            </Nav.Link>
                        </Link>
                        <Link href="/first" passHref>
                            <Nav.Link>
                                First
                            </Nav.Link>
                        </Link>

                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}